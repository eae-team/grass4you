/* eslint-disable */

// Configuration file for all things Slate.
// For more information, visit https://github.com/Shopify/slate/wiki/Slate-Configuration

const path = require('path');

module.exports = {
  'cssVarLoader.liquidPath': ['src/snippets/css-variables.liquid'],
  'webpack.extend': {
    resolve: {
      alias: {
        jquery: path.resolve('./node_modules/jquery'),
        'lodash-es': path.resolve('./node_modules/lodash-es'),
      },
    },
    module: { 
      rules: [
        // .. other rules
        {
          test: /\.(svg|jpeg|gif|png|jpg)$/, // lightbox2 requires png and gif images to be imported
                                             // ensure `node_modules` folder is not excluded
          include: [/node_modules\/lightbox2/],
          use: {
            loader: 'file-loader',
            options: {
              name: "[name].[ext]",
              outputPath: ""
            }
          },
        },
        // .. yet other rules
      ]
    }
  },
};
