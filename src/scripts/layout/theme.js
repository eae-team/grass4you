import AOS from 'aos';
import {focusHash, bindInPageLinks} from '@shopify/theme-a11y';
import lightbox from '../../../node_modules/lightbox2/dist/js/lightbox';
import { load } from '@shopify/theme-sections';

import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';
import '../sections/header';
import '../sections/footer';
import '../components/quick-cart';

// Common a11y fixes
focusHash();
bindInPageLinks();

// Apply a specific class to the html element for browser support of cookies.
if (window.navigator.cookieEnabled) {
  document.documentElement.className = document.documentElement.className.replace(
    'supports-no-cookies',
    'supports-cookies',
  );
}

AOS.init({
  easing: 'ease-out',
  delay: 300,
  duration: 700,
  once: true,
});

lightbox.option({
  // disableScrolling: true,
  fitImagesInViewport: true,
  fadeDuration: 400,
  imageFadeDuration: 200,
  resizeDuration: 200,
});

function checkAnnouncementBar() {
  const announcementBar = document.querySelector('[data-role="announcement-bar"]');
  if (!announcementBar) {
    return;
  }
  setTimeout(() => {
    document.body.classList.add('_show-announcement-bar');
  }, 3000);
}
checkAnnouncementBar();

load('*');
