import { register } from '@shopify/theme-sections';
import { sliderProducts } from '../components/products-slider';

const selectors = {
  swiperProduct: '.products-slider__slider-container',
};

register('products-slider-about', {
  onLoad() {
    this.swiperProduct = sliderProducts(selectors.swiperProduct);
  },

  onUnload() {
    this.swiperProduct.destroy();
  },
});

