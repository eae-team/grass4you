import { register } from '@shopify/theme-sections';
import { sliderBanner } from '../components/header-banner';
import { sliderBannerMobile } from '../components/mobile-header-banner';
import { breaks } from '../components/utils';

const selectors = {
  swiperHeaderBanner: '.header-banner__slider-container',
  swiperServices: '.header-banner__description-column',
};

register('header-banner-section', {

  onLoad() {
    this.swiperHeaderBanner = sliderBanner(selectors.swiperHeaderBanner);
    this.swiperServices = null;
    this.resize = this.resize.bind(this);
    window.onresize = this.resize;
    window.dispatchEvent(new Event('resize'));
    setTimeout(() => {
      this.container.classList.add('animate');
    }, 2000);
  },

  resize(event) {
    const width = event.target.innerWidth;
    if (width <= breaks.medium && !this.swiperServices) {
      this.swiperServices = sliderBannerMobile(selectors.swiperServices);
    } else if (width > breaks.medium && this.swiperServices) {
      this.swiperServices.destroy(true, true);
      this.swiperServices = null;
    }
  },

  onUnload() {
    this.swiperHeaderBanner.destroy(true, true);
  },
});
