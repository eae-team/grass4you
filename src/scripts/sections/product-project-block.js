import { register } from '@shopify/theme-sections';
import Swiper, { Navigation, Pagination } from 'swiper';
import { breaks } from '../components/utils';

const selectors = {
  sliderScope: '[data-role="product-project-block"]',
  swiperProjects: '.block-project-media__container',
};

register('product-project-block', {
  onLoad() {
    const windowWidth = window.innerWidth;
    this.sliderSwiper = this.sliderSwiper.bind(this);
    this.swiperProjects = null;

    if (windowWidth <= breaks.medium) {
      this.swiperProjects = this.sliderSwiper(selectors.swiperProjects);
    }

    this.resize = this.resize.bind(this);

    window.onresize = this.resize;
    window.dispatchEvent(new Event('resize'));
  },

  sliderSwiper(element) {
    Swiper.use([Navigation, Pagination]);
    return new Swiper(element, {
      slidesPerView: 1.25,
      spaceBetween: 5,
      initialSlide: 1,
      centeredSlides: true,
      autoHeight: true,
      breakpoints: {
        500: {
          slidesPerView: 1.5,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 2.5,
          spaceBetween: 20,
        },
      },
    });
  },

  resize(event) {
    const width = event.target.innerWidth;
    if (width <= breaks.medium && !this.swiperProjects) {
      this.swiperProjects = this.sliderSwiper(selectors.swiperProjects);
    } else if (width > breaks.medium && this.swiperProjects) {
      this.swiperProjects.destroy(true, true);
      this.swiperProjects = null;
    }
  },

  onUnload() {
    this.swiperHeaderBanner.destroy(true, true);
  },
});
