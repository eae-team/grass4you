import { register } from '@shopify/theme-sections';
import { breaks } from '../components/utils';

const footerSelectors = {
  footerScope: '[data-role="footer"]',
  footerColumn1: '[data-action="extra-1"]',
  footerColumn2: '[data-action="extra-2"]',
  footerColumn3: '[data-action="extra-3"]',
  footerRoleColumn1: '[data-role="extra-1"]',
  footerRoleColumn2: '[data-role="extra-2"]',
  footerRoleColumn3: '[data-role="extra-3"]',
  footerCollapse: '[data-action="open-footer"]',
  footerOpenMobile: '[data-role="open-mobile"]',
};

register('footer', {
  onLoad() {

    const footerExtraActionFirst = this.container.querySelector(footerSelectors.footerColumn1);
    const footerExtraActionSecond = this.container.querySelector(footerSelectors.footerColumn2);
    const footerExtraActionThird = this.container.querySelector(footerSelectors.footerColumn3);
    const footerCollapseAll = this.container.querySelectorAll(footerSelectors.footerCollapse);

    this.toogleFooter = this.toogleFooter.bind(this);
    this.onFooterMobileTrigger = this.onFooterMobileTrigger.bind(this);

    if (footerExtraActionFirst) {
      footerExtraActionFirst.addEventListener('click', (event) => {
        event.preventDefault();
        const trigger = event.target;
        const extraSelector = this.container.querySelector(footerSelectors.footerRoleColumn1);
        this.toogleFooter(extraSelector, trigger);
      });
    }
    if (footerExtraActionSecond) {
      footerExtraActionSecond.addEventListener('click', (event) => {
        event.preventDefault();
        const trigger = event.target;
        const extraSelector = this.container.querySelector(footerSelectors.footerRoleColumn2);
        this.toogleFooter(extraSelector, trigger);
      });
    }
    if (footerExtraActionThird) {
      footerExtraActionThird.addEventListener('click', (event) => {
        event.preventDefault();
        const trigger = event.target;
        const extraSelector = this.container.querySelector(footerSelectors.footerRoleColumn3);
        this.toogleFooter(extraSelector, trigger);
      });
    }

    footerCollapseAll.forEach((item) => {
      item.addEventListener('click', this.onFooterMobileTrigger);
    });

  },

  onFooterMobileTrigger(event) {
    if (window.innerWidth >= breaks.medium) {
      return;
    }
    event.preventDefault();
    const target = event.target;
    const containerInfo = target.parentNode.querySelector(footerSelectors.footerOpenMobile);

    if (target.classList.contains('_open')) {
      target.classList.remove('_open');
      containerInfo.style.height = '';
      containerInfo.style.opacity = '';
    } else {
      target.classList.add('_open');
      containerInfo.style.height = 'auto';
      const targetHeight = `${containerInfo.scrollHeight}px`;
      containerInfo.style.height = '0px';

      setTimeout(() => {
        containerInfo.style.height = targetHeight;
        containerInfo.style.opacity = 1;
      }, 100);
    }
  },

  toogleFooter(currentColumn, trigger) {

    if (currentColumn.classList.contains('_open')) {
      currentColumn.classList.remove('_open');
      trigger.querySelector('span').textContent = '+';
      currentColumn.style.height = '0px';
    } else {
      currentColumn.classList.add('_open');
      currentColumn.style.height = 'auto';
      const targetHeight = `${currentColumn.scrollHeight}px`;
      trigger.querySelector('span').textContent = '-';
      currentColumn.style.height = '0px';
      setTimeout(() => {
        currentColumn.style.height = targetHeight;
      }, 100);
    }
  },

  onUnload() {},
});
