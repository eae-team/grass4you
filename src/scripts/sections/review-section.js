import { register } from '@shopify/theme-sections';
import { sliderReviews } from '../components/reviews-slider';

const selectors = {
  swiperReview: '.review-slider__slider-container',
};

register('review-section', {
  onLoad() {
    this.swiperReview = sliderReviews(selectors.swiperReview);
  },

  onUnload() {
    this.swiperReview.destroy();
  },
});

