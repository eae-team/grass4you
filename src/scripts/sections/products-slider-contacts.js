import { register } from '@shopify/theme-sections';
import { sliderProducts } from '../components/products-slider';

const selectors = {
  productsScope: '[data-role="products-slider-contacts"]',
  swiperProduct: '.products-slider__slider-container',
};

register('products-slider-contacts', {
  onLoad() {
    this.swiperProduct = sliderProducts(selectors.swiperProduct);
  },

  onUnload() {
    this.swiperProduct.destroy();
  },
});
