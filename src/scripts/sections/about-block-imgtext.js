import { register } from '@shopify/theme-sections';
import { breaks } from '../components/utils';

const selectors = {
  aboutOpenInfoButton: '[data-action="open-info"]',
  aboutContainerInfo: '[data-role="container-info"]',
};

register('about-block-imgtext', {
  onLoad() {
    this.flagResize = true;
    const aboutOpenInfoButton = this.container.querySelectorAll(selectors.aboutOpenInfoButton);
    this.resize = this.resize.bind(this);
    this.onOpenInfo = this.onOpenInfo.bind(this);
    window.onresize = this.resize;
    window.dispatchEvent(new Event('resize'));
    aboutOpenInfoButton.forEach((item) => {
      item.addEventListener('click', this.onOpenInfo);
    });
  },

  onOpenInfo(event) {
    if (window.innerWidth >= breaks.medium) {
      return;
    }
    event.preventDefault();
    const target = event.target;
    const containerInfo = target.parentNode.querySelector(selectors.aboutContainerInfo);

    if (target.classList.contains('_open')) {
      target.classList.remove('_open');
      containerInfo.style.height = '';
      containerInfo.style.opacity = '';
    } else {
      target.classList.add('_open');
      containerInfo.style.height = 'auto';
      const targetHeight = `${containerInfo.scrollHeight}px`;
      containerInfo.style.height = '0px';
  
      setTimeout(() => {
        containerInfo.style.height = targetHeight;
        containerInfo.style.opacity = 1;
      }, 100);
    }
  },

  resize(event) {
    const width = event.target.innerWidth;
    if (width > breaks.medium && this.flagResize) {
      const allItems = this.container.querySelectorAll(selectors.aboutContainerInfo);
      allItems.forEach((item) => {
        item.style.height = '';
        item.style.opacity = '';
      });
      const aboutOpenInfoButton = this.container.querySelectorAll(selectors.aboutOpenInfoButton);
      aboutOpenInfoButton.forEach((item) => {
        item.classList.remove('_open');
      });
      this.flagResize = false;
    } else if (width <= breaks.medium && !this.flagResize) {
      this.flagResize = true;
    }
  },

  onUnload() {
  },
});
