import Headroom from 'headroom.js';
import { register } from '@shopify/theme-sections';
import { breaks, toggleMenu, closeMenu, blockContent, unblockContent } from '../components/utils';

const selectors = {
  headerScope: '[data-role="header-menu"]',
  headerSubmenu: '[data-role="submenu-trigger"]',
  headerDropmenu: '[data-role="dropmenu-trigger"]',
  headerBurgerButton: '[data-action="toggle-mobile-menu"]',
  headerCloseButton: '[data-action="close-mobile-menu"]',
  headerOverlay: '[data-role="menu-overlay"]',
  dropmenu: '[data-role="dropmenu"]',
  submenu: '[data-role="submenu"]',
  fold: '[data-role="menu-fold"]',
  budgetButton: '[data-action-ask-budget]',
  modalContainer: '[data-role-modal-box]',
  modalClose: '[data-close-modal-box]',
};

register('header', {
  onLoad() {
    this.headerBodyTag = document.body;
    this.headerHtmlTag = document.html;
    this.headerScope = document.querySelector(selectors.headerScope);
    this.headerHeadroom = new Headroom(this.headerScope, { offset: 150 });
    this.headerHeadroom.init();
    window.headroom = this.headerHeadroom;
    this.menuTimeout = null;
    this.dropTimeout = null;

    const headerSubmenu = this.container.querySelector(selectors.headerSubmenu);
    const headerDropmenu = this.container.querySelectorAll(selectors.headerDropmenu);
    const headerBurgerButton = this.container.querySelector(selectors.headerBurgerButton);
    const headerCloseButton = this.container.querySelector(selectors.headerCloseButton);
    const budgetButton = this.container.querySelectorAll(selectors.budgetButton);
    const headerOverlay = this.container.querySelector(selectors.headerOverlay);
    const submenuTrigger = this.container.querySelector('[data-role="submenu-trigger"] > a');
    const dropmenuTrigger = this.container.querySelectorAll('[data-role="dropmenu-trigger"] > a');

    this.onDropmenuEnter = this.onDropmenuEnter.bind(this);
    this.onDropmenuLeave = this.onDropmenuLeave.bind(this);
    this.onSubmenuEnter = this.onSubmenuEnter.bind(this);
    this.onSubmenuLeave = this.onSubmenuLeave.bind(this);
    this.onToggleMenu = this.onToggleMenu.bind(this);
    this.onCloseMenu = this.onCloseMenu.bind(this);
    this.onSubmenuTrigger = this.onSubmenuTrigger.bind(this);
    this.onDropmenuTrigger = this.onDropmenuTrigger.bind(this);
    this.onOpenBudget = this.onOpenBudget.bind(this);
    this.onModalClose = this.onModalClose.bind(this);

    if (headerDropmenu && headerDropmenu.length > 0) {
      headerDropmenu.forEach((item) => {
        item.addEventListener('mouseenter', this.onDropmenuEnter);
        item.addEventListener('mouseleave', this.onDropmenuLeave);
      });
    }
    if (headerSubmenu) { headerSubmenu.addEventListener('mouseenter', this.onSubmenuEnter); }
    if (headerSubmenu) { headerSubmenu.addEventListener('mouseleave', this.onSubmenuLeave); }
    if (headerCloseButton) { headerCloseButton.addEventListener('click', this.onToggleMenu); }
    if (headerBurgerButton) { headerBurgerButton.addEventListener('click', this.onToggleMenu); }
    if (headerOverlay) { headerOverlay.addEventListener('click', this.onCloseMenu); }
    if (submenuTrigger) { submenuTrigger.addEventListener('click', this.onSubmenuTrigger); }
    if (dropmenuTrigger && dropmenuTrigger.length > 0) {
      dropmenuTrigger.forEach((item) => {
        item.addEventListener('click', this.onDropmenuTrigger);
      });
    }
    if (budgetButton && budgetButton.length > 0) {
      budgetButton.forEach((item) => {
        item.addEventListener('click', this.onOpenBudget);
      });
    }

    document.addEventListener('click', (event) => {
      if (event.target.closest(selectors.modalClose)) {
        this.onModalClose(event);
      }
    });
  },

  onModalClose() {
    unblockContent();
    const modalContainer = this.container.querySelector(selectors.modalContainer);
    modalContainer.classList.add('_hide');
    let childrens = modalContainer.innerHTML;
    const filtered = childrens.replace(/(<iframe.*?>.*?<\/iframe>)/g, '');
    childrens = filtered;
    modalContainer.innerHTML = childrens;
  },

  onOpenBudget() {
    const modalContainer = this.container.querySelector(selectors.modalContainer);
    const iframe = document.createElement('iframe');
    iframe.style.width = '100%';
    iframe.style.height = '100%';
    iframe.src = modalContainer.dataset.embed;
    modalContainer.appendChild(iframe);
    closeMenu();
    blockContent();
    modalContainer.classList.remove('_hide');
  },

  onDropmenuTrigger(event) {
    if (window.innerWidth >= breaks.medium) {
      if (this.headerHtmlTag.classList.contains('_touch')) {
        event.preventDefault();
      }
      return;
    }
    event.preventDefault();
    const menu = event.target.closest(selectors.headerDropmenu);
    const dropmenu = menu.querySelector(selectors.dropmenu);
    if (menu.classList.contains('_open')) {
      menu.classList.remove('_open');
      dropmenu.style.height = 0;
    } else {
      menu.classList.add('_open');
      dropmenu.style.height = 'auto';
      const targetHeight = `${dropmenu.scrollHeight}px`;
      dropmenu.style.height = '0px';

      setTimeout(() => {
        dropmenu.style.height = targetHeight;
      }, 100);
    }
  },

  onSubmenuTrigger(event) {
    if (window.innerWidth >= breaks.medium) {
      if (this.headerHtmlTag.classList.contains('_touch')) {
        event.preventDefault();
      }
      return;
    }
    event.preventDefault();
    const menu = event.target.closest(selectors.headerSubmenu);
    const submenu = menu.querySelector(selectors.submenu);
    if (menu.classList.contains('_open')) {
      menu.classList.remove('_open');
      submenu.style.height = 0;
    } else {
      menu.classList.add('_open');
      submenu.style.height = 'auto';
      const targetHeight = `${submenu.scrollHeight}px`;
      submenu.style.height = '0px';

      setTimeout(() => {
        submenu.style.height = targetHeight;
      }, 100);
    }
  },

  onCloseMenu(event) {
    event.preventDefault();
    closeMenu();
  },

  onToggleMenu(event) {
    event.preventDefault();
    toggleMenu();
  },

  onSubmenuLeave(event) {
    const submenu = event.target.querySelector(selectors.submenu);
    submenu.classList.remove('active');
    this.menuTimeout = setTimeout(() => {
      this.headerBodyTag.classList.remove('_overlay-active');
      const fold = this.container.querySelector(selectors.fold);
      fold.style.height = '0px';
    }, 200);
  },

  onSubmenuEnter(event) {
    if (window.innerWidth < breaks.medium) {
      return;
    }

    // Prevents incosistent behavior if the menu is open before last close timeout
    if (this.menuTimeout) {
      clearTimeout(this.menuTimeout);
    }
    const submenu = event.target.querySelector(selectors.submenu);
    const menuHeight = submenu.clientHeight - 40;
    submenu.classList.add('active');
    this.headerBodyTag.classList.add('_overlay-active');
    const fold = this.container.querySelector(selectors.fold);
    fold.style.height = `${menuHeight}px`;
  },

  onDropmenuLeave(event) {
    const target = event.target;
    const dropmenu = target.querySelector(selectors.dropmenu);
    dropmenu.classList.remove('active');
    this.dropTimeout = setTimeout(() => {
      this.headerBodyTag.classList.remove('_overlay-active');
    }, 200);
  },

  onDropmenuEnter(event) {
    if (window.innerWidth < breaks.medium) {
      return;
    }

    // Prevents incosistent behavior if the menu is open before last close timeout
    if (this.dropTimeout) {
      clearTimeout(this.dropTimeout);
    }
    const target = event.target;
    const dropmenu = target.querySelector(selectors.dropmenu);
    dropmenu.classList.add('active');
    this.headerBodyTag.classList.add('_overlay-active');
  },

  onUnload() {
    this.headerHeadroom.destroy();
    window.headroom = null;
  },
});
