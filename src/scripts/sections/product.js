/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

import { getUrlWithVariant, ProductForm } from '@shopify/theme-product-form';
import { formatMoney } from '@shopify/theme-currency';
import { register } from '@shopify/theme-sections';
import { forceFocus } from '@shopify/theme-a11y';
import Swiper, { Navigation, Pagination } from 'swiper';
import { addToCart, openCart, closeCart } from '../components/cart-actions';
import { breaks } from '../components/utils';

const classes = {
  hide: 'hide',
};

const keyboardKeys = {
  ENTER: 13,
};

const selectors = {
  submitButton: '[data-submit-button]',
  submitButtonText: '[data-submit-button-text]',
  comparePrice: '[data-compare-price]',
  comparePriceText: '[data-compare-text]',
  priceWrapper: '[data-price-wrapper]',
  imageWrapper: '[data-product-image-wrapper]',
  visibleImageWrapper: `[data-product-image-wrapper]:not(.${classes.hide})`,
  imageWrapperById: (id) => `${selectors.imageWrapper}[data-image-id='${id}']`,
  productForm: '[data-product-form]',
  productPrice: '[data-product-price]',
  thumbnail: '[data-product-single-thumbnail]',
  thumbnailById: (id) => `[data-thumbnail-id='${id}']`,
  thumbnailActive: '[data-product-single-thumbnail][aria-current]',
  quantityInput: '[data-role="quantity"]',
  quantityUp: '[data-action="quantity-up"]',
  quantityDown: '[data-action="quantity-down"]',
  openTabs: '[data-action="open-tab"]',
  tabContent: '[data-role="tab-content"]',
  openLightbox: '[data-action-open-lightbox]',
  closeLightbox: '[data-action="close-lightbox"]',
  lightbox: '[data-role="lightbox"]',
  accordion: '[data-action="colapsible-toggle"]',
};

register('product', {
  async onLoad() {
    Swiper.use([Navigation, Pagination]);
    this.swiperLightbox = new Swiper('.product__lightbox-container', {
      slidesPerView: 1,
      centeredSlides: true,
      spaceBetween: 0,
      loop: true,
      navigation: {
        nextEl: '.product__lightbox-next',
        prevEl: '.product__lightbox-prev',
      },
      pagination: {
        el: '.product__lightbox-pagination',
        type: 'bullets',
        bulletActiveClass: 'product__bullet-active',
        bulletClass: 'product__bullet',
        clickable: true,
      },
    });

    const productFormElement = document.querySelector(selectors.productForm);
    const quantityUp = this.container.querySelector(selectors.quantityUp);
    const quantityDown = this.container.querySelector(selectors.quantityDown);
    const quantityInput = this.container.querySelector(selectors.quantityInput);
    const openTabs = this.container.querySelectorAll(selectors.openTabs);
    const openLightbox = this.container.querySelectorAll(selectors.openLightbox);
    const closeLightbox = this.container.querySelectorAll(selectors.closeLightbox);
    const faq = this.container.querySelectorAll(selectors.accordion);
    const windowWidth = window.innerWidth;

    this.product = await this.getProductJson(
      productFormElement.dataset.productHandle,
    );
    this.productForm = new ProductForm(productFormElement, this.product, {
      onOptionChange: this.onFormOptionChange.bind(this),
    });

    this.onThumbnailClick = this.onThumbnailClick.bind(this);
    this.onThumbnailKeyup = this.onThumbnailKeyup.bind(this);
    this.onQuantityClick = this.onQuantityClick.bind(this);
    this.onQuantityChange = this.onQuantityChange.bind(this);
    this.onOpenTab = this.onOpenTab.bind(this);
    this.onOpenLightbox = this.onOpenLightbox.bind(this);
    this.onCloseLightbox = this.onCloseLightbox.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);

    this.container.addEventListener('click', this.onThumbnailClick);
    this.container.addEventListener('keyup', this.onThumbnailKeyup);
    this.container.addEventListener('submit', this.onSubmitForm);
    quantityUp.addEventListener('click', (event) => this.onQuantityClick(event, 1));
    quantityDown.addEventListener('click', (event) => this.onQuantityClick(event, -1));
    quantityInput.addEventListener('change', this.onQuantityChange);

    openLightbox.forEach((tab) => {
      tab.addEventListener('click', this.onOpenLightbox);
    });

    closeLightbox.forEach((tab) => {
      tab.addEventListener('click', this.onCloseLightbox);
    });

    openTabs.forEach((tab) => {
      tab.addEventListener('click', this.onOpenTab);
    });

    for (let i = 0; i < faq.length; i++) {
      faq[i].addEventListener('click', (event) => {
        event.preventDefault();
        const _this = event.currentTarget;
        const answer = _this.nextElementSibling;
        if (_this.classList.contains('_active')) {
          _this.classList.remove('_active');
          answer.style.maxHeight = null;
          _this.querySelector('.colapsible-title div .open').classList.add('_active');
          _this.querySelector('.colapsible-title div .close').classList.remove('_active');
        } else {
          const active = document.querySelectorAll('.accordion._active');
          if (active.length > 0) {
            active.forEach((ele) => {
              ele.classList.remove('_active');
              ele.nextElementSibling.style.maxHeight = null;
              ele.querySelector('.colapsible-title div .open').classList.add('_active');
              ele.querySelector('.colapsible-title div .close').classList.remove('_active');
            });
          }
          _this.classList.add('_active');
          answer.style.maxHeight = `${answer.scrollHeight}px`;
          _this.querySelector('.colapsible-title div .open').classList.remove('_active');
          _this.querySelector('.colapsible-title div .close').classList.add('_active');
        }
      });
    }
  },

  onCloseLightbox(event) {
    const body = document.body;
    const lightbox = this.container.querySelector(selectors.lightbox);
    if (lightbox.classList.contains('_show')) {
      body.classList.remove('_open-modal', '_block-content');
      lightbox.classList.remove('_show');
    }
  },

  onOpenLightbox(event) {
    const element = event.target;
    const body = document.body;
    const lightbox = this.container.querySelector(selectors.lightbox);
    if (!lightbox.classList.contains('_show')) {
      this.swiperLightbox.slideTo(element.dataset.imagePos);
      setTimeout(() => {
        body.classList.add('_open-modal', '_block-content');
        lightbox.classList.add('_show');
      }, 300);
    }
  },

  onOpenTab(event) {
    const element = event.currentTarget;

    if (!element.classList.contains('_active-tab')) {
      const openTabs = this.container.querySelectorAll(selectors.openTabs);
      const tabContent = this.container.querySelectorAll(selectors.tabContent);

      openTabs.forEach((tab) => {
        if (tab.classList.contains('mobile') && tab.classList.contains('_active-tab')) {
          tab.querySelector('div .open').classList.add('_active-tab');
          tab.querySelector('div .close').classList.remove('_active-tab');
        }
        tab.classList.remove('_active-tab');
      });

      tabContent.forEach((content) => {
        content.classList.add('_hide');
      });

      const attribute = element.dataset.tabId;
      const clickable = document.querySelectorAll(`[data-tab-id='${attribute}']`);
      clickable.forEach((el) => {
        el.classList.add('_active-tab');
        if (el.classList.contains('mobile') && el.classList.contains('_active-tab')) {
          el.querySelector('div .open').classList.remove('_active-tab');
          el.querySelector('div .close').classList.add('_active-tab');
        }
      });
      tabContent[element.dataset.tabId].classList.remove('_hide');
    }
  },

  onQuantityChange() {
    const quantityInput = this.container.querySelector(selectors.quantityInput);
    const currentValue = parseInt(quantityInput.value, 10);

    let newVal = currentValue;
    if ((currentValue) < 1) {
      newVal = 1;
    }
    quantityInput.value = newVal;
  },

  onQuantityClick(event, quantity) {
    const quantityInput = this.container.querySelector(selectors.quantityInput);
    const currentValue = parseInt(quantityInput.value, 10);

    let newVal;
    if ((currentValue + quantity) < 1) {
      newVal = 1;
    } else {
      newVal = currentValue + quantity;
    }
    quantityInput.value = newVal;
  },

  onUnload() {
    this.productForm.destroy();
    this.removeEventListener('click', this.onThumbnailClick);
    this.removeEventListener('keyup', this.onThumbnailKeyup);
  },

  getProductJson(handle) {
    return fetch(`/products/${handle}.js`).then((response) => {
      return response.json();
    });
  },

  onFormOptionChange(event) {
    const variant = event.dataset.variant;

    this.renderImages(variant);
    this.renderPrice(variant);
    this.renderComparePrice(variant);
    this.renderSubmitButton(variant);

    this.updateBrowserHistory(variant);
  },

  onThumbnailClick(event) {
    const thumbnail = event.target.closest(selectors.thumbnail);

    if (!thumbnail) {
      return;
    }

    event.preventDefault();

    this.renderFeaturedImage(thumbnail.dataset.thumbnailId);
    this.renderActiveThumbnail(thumbnail.dataset.thumbnailId);
  },

  onThumbnailKeyup(event) {
    if (
      event.keyCode !== keyboardKeys.ENTER ||
      !event.target.closest(selectors.thumbnail)
    ) {
      return;
    }

    const visibleFeaturedImageWrapper = this.container.querySelector(
      selectors.visibleImageWrapper,
    );

    forceFocus(visibleFeaturedImageWrapper);
  },

  onSubmitForm(event) {
    event.preventDefault();
    const form = this.container.querySelector('[data-product-form]');
    const quantity = form.querySelector('input[name="quantity"]').value;
    const variantId = this.productForm.variant().id || this.product.id;
    const propertiesInputs = form.querySelectorAll('[name^="properties"]');
    const properties = {};
    propertiesInputs.forEach((input) => {
      properties[input.name.match(/properties\[(.*)\]/)[1]] = input.value;
    });
    form.classList.add('_loading');
    addToCart(variantId, quantity, properties)
      .then((isSuccess) => {
        if (isSuccess) {
          form.classList.add('_added-to-cart');
          setTimeout(() => {
            openCart();
          }, 500);

          // setTimeout(() => {
          //   form.classList.remove('_added-to-cart');
          //   closeCart();
          // }, 5000);
        } else if (isSuccess === 0) {
          form.classList.add('_not-available');
          form.querySelector(selectors.submitButton).disabled = true;
        }
        form.classList.remove('_loading');
        return true;
      })
      .catch(() => {
        form.classList.remove('_loading');
        return false;
      });
  },

  renderSubmitButton(variant) {
    const submitButton = this.container.querySelector(selectors.submitButton);
    const submitButtonText = this.container.querySelector(
      selectors.submitButtonText,
    );

    if (!variant) {
      submitButton.disabled = true;
      submitButtonText.innerText = theme.strings.unavailable;
    } else if (variant.available) {
      submitButton.disabled = false;
      submitButtonText.innerText = theme.strings.addToCart;
    } else {
      submitButton.disabled = true;
      submitButtonText.innerText = theme.strings.soldOut;
    }
  },

  renderImages(variant) {
    if (!variant || variant.featured_image === null) {
      return;
    }

    this.renderFeaturedImage(variant.featured_image.id);
    this.renderActiveThumbnail(variant.featured_image.id);
  },

  renderPrice(variant) {
    const priceElement = this.container.querySelector(selectors.productPrice);
    const priceWrapperElement = this.container.querySelector(
      selectors.priceWrapper,
    );

    priceWrapperElement.classList.toggle(classes.hide, !variant);

    if (variant) {
      priceElement.innerText = formatMoney(variant.price, theme.moneyFormat);
    }
  },

  renderComparePrice(variant) {
    if (!variant) {
      return;
    }

    const comparePriceElement = this.container.querySelector(
      selectors.comparePrice,
    );
    const compareTextElement = this.container.querySelector(
      selectors.comparePriceText,
    );

    if (!comparePriceElement || !compareTextElement) {
      return;
    }

    if (variant.compare_at_price > variant.price) {
      comparePriceElement.innerText = formatMoney(
        variant.compare_at_price,
        theme.moneyFormat,
      );
      compareTextElement.classList.remove(classes.hide);
      comparePriceElement.classList.remove(classes.hide);
    } else {
      comparePriceElement.innerText = '';
      compareTextElement.classList.add(classes.hide);
      comparePriceElement.classList.add(classes.hide);
    }
  },

  renderActiveThumbnail(id) {
    const activeThumbnail = this.container.querySelector(
      selectors.thumbnailById(id),
    );
    const inactiveThumbnail = this.container.querySelector(
      selectors.thumbnailActive,
    );

    if (activeThumbnail === inactiveThumbnail) {
      return;
    }

    inactiveThumbnail.removeAttribute('aria-current');
    activeThumbnail.setAttribute('aria-current', true);
    inactiveThumbnail.classList.remove('_active');
    activeThumbnail.classList.add('_active');
  },

  renderFeaturedImage(id) {
    const activeImage = this.container.querySelector(
      selectors.visibleImageWrapper,
    );
    const inactiveImage = this.container.querySelector(
      selectors.imageWrapperById(id),
    );

    activeImage.classList.add(classes.hide);
    inactiveImage.classList.remove(classes.hide);
  },

  updateBrowserHistory(variant) {
    const enableHistoryState = this.productForm.element.dataset
      .enableHistoryState;

    if (!variant || enableHistoryState !== 'true') {
      return;
    }

    const url = getUrlWithVariant(window.location.href, variant.id);
    window.history.replaceState({ path: url }, '', url);
  },
});
