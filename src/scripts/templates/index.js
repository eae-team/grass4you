import { load } from '@shopify/theme-sections';
import '../sections/product';
import '../sections/header-banner-section';
import '../sections/review-section';
import '../sections/hp-block-imgtext';

load('*');
