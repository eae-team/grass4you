import '../sections/product-project-block';
import { ArticleBlogMobile } from '../components/article-blog';
import { breaks } from '../components/utils';
import { load } from '@shopify/theme-sections';

load('*');

window.addEventListener('load', () => {
  const wrapper = document.querySelector("[data-role='article-categories']");
  const catList = document.querySelector('.article-categories');
  const tags = document.querySelectorAll('.blog-tag');
  const init = document.querySelector('.article-block__tags .init');
  const tag = window.localStorage.getItem('tag');
  const swiperBlogContainer = document.querySelector('.related-articles__blog');
  const width = window.innerWidth;
  let swiperBlog = null;
  if (width <= breaks.medium && !swiperBlog) {
    swiperBlog = ArticleBlogMobile(swiperBlogContainer);
  }

  if (tag) {

    const active = document.querySelector(`[data-tag="${tag}"]`);
    active.classList.add('_active');

  } else {

    const allTags = document.querySelectorAll('[data-tagheader]');

    allTags.forEach((ele) => {

      const tagList = document.querySelectorAll('[data-tag]');
      let i;
      for (i = 0; i < tagList.length; i++) {
        if (ele.innerHTML === tagList[i].dataset.tag) {
          tagList[i].classList.add('_active');
        }
      }
    });
  }

  window.localStorage.clear();

  wrapper.addEventListener('click', (e) => {
    const container = e.currentTarget;

    if (container.classList.contains('open')) {
      catList.style.maxHeight = null;
      container.classList.remove('open');

    } else {
      catList.style.maxHeight = `${catList.scrollHeight}px`;
      container.classList.add('open');
    }
  });

  window.addEventListener('resize', (e) => {
    catList.style.maxHeight = null;
    wrapper.classList.remove('open');
    const width = e.target.innerWidth;
    if (width <= breaks.medium && !swiperBlog) {
      swiperBlog = ArticleBlogMobile(swiperBlogContainer);
    } else if (width > breaks.medium && swiperBlog) {
      swiperBlog.destroy(true, true);
      swiperBlog = null;
    }
  });

  // tags.forEach((el) => {
  //   el.addEventListener('click', (e) => {
  //     const _this = e.target;
  //     const text = _this.textContent;
  //     init.textContent = text;
  //   });
  // });
});
