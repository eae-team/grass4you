import '../sections/product-project-block';
import { ArticleBlogMobile } from '../components/article-blog';
import { breaks } from '../components/utils';
import { load } from '@shopify/theme-sections';

load('*');

window.addEventListener('load', () => {
  const wrapper = document.querySelector("[data-role='blog-categories']");
  const catList = document.querySelector('.blog-categories');
  const tags = document.querySelectorAll('.blog-tag');
  const init = document.querySelector('.blog-block__tags .init');
  const images = document.querySelectorAll('.blog-block__article-image');
  const titles = document.querySelectorAll('.blog-block__article-title');
  const btns = document.querySelectorAll('.blog-block__article-more');
  const swiperBlogContainer = document.querySelector('.related-articles__blog');
  const width = window.innerWidth;
  let swiperBlog = null;
  if (width <= breaks.medium && !swiperBlog) {
    swiperBlog = ArticleBlogMobile(swiperBlogContainer);
  }

  images.forEach((ele) => {
    ele.addEventListener('click', saveToLocalStorage);
  });

  titles.forEach((ele) => {
    ele.addEventListener('click', saveToLocalStorage);
  });

  btns.forEach((ele) => {
    ele.addEventListener('click', saveToLocalStorage);
  });

  function saveToLocalStorage() {
    const tag = document.querySelector('.blog-block__tags ._active');
    if (tag != null) {
      window.localStorage.setItem('tag', tag.dataset.tag);
    }
  }

  wrapper.addEventListener('click', (e) => {
    const container = e.currentTarget;

    if (container.classList.contains('open')) {
      catList.style.maxHeight = null;
      container.classList.remove('open');

    } else {
      catList.style.maxHeight = `${catList.scrollHeight}px`;
      container.classList.add('open');
    }
  });

  window.addEventListener('resize', (e) => {
    catList.style.maxHeight = null;
    wrapper.classList.remove('open');
    const width = e.target.innerWidth;
    if (width <= breaks.medium && !swiperBlog) {
      swiperBlog = ArticleBlogMobile(swiperBlogContainer);
    } else if (width > breaks.medium && swiperBlog) {
      swiperBlog.destroy(true, true);
      swiperBlog = null;
    }
  });

  // tags.forEach((el) => {
  //   el.addEventListener('click', (e) => {
  //     const _this = e.target;
  //     const text = _this.textContent;
  //     init.textContent = text;
  //   });
  // });
});
