import { load } from '@shopify/theme-sections';
import '../sections/about-block-imgtext';
import '../sections/products-slider-about';
import '../sections/review-section';

load('*');
