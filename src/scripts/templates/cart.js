import * as cartActions from '../components/cart-actions';

const cartSelectors = {
  cartScope: '[data-role="cart"]',
  cartQuantityDown: '[data-action="cart-quantity-down"]',
  cartQuantityUp: '[data-action="cart-quantity-up"]',
  cartQuantity: '[data-role="cart-quantity"]',
};

const cartScope = document.querySelector(cartSelectors.cartScope);

function onQuantityClick(event, quantity) {
  const target = event.target;
  const productId = target.dataset.id;
  const quantityInput = cartScope.querySelector(`[data-cart-product-id="${productId}"]`);
  const currentValue = parseInt(quantityInput.value, 10);
  let newVal;
  if ((currentValue + quantity) < 1) {
    newVal = 1;
  } else {
    newVal = currentValue + quantity;
  }
  quantityInput.value = newVal;

  const productKey = quantityInput.dataset.cartKey;
  cartActions
    .updateItemCart(productKey, newVal, {})
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.log(error);
    });
}

function onQuantityChange(event) {
  const quantityInput = event.target;
  const currentValue = parseInt(quantityInput.value, 10);
  let newVal = currentValue;
  if ((currentValue) < 1) {
    newVal = 1;
  }
  quantityInput.value = newVal;

  const productKey = quantityInput.dataset.cartKey;
  cartActions
    .updateItemCart(productKey, newVal, {})
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.log(error);
    });
}

document.addEventListener('click', (event) => {
  if (event.target.closest(cartSelectors.cartQuantityDown)) {
    onQuantityClick(event, -1);
  } else if (event.target.closest(cartSelectors.cartQuantityUp)) {
    onQuantityClick(event, 1);
  }
});

document.addEventListener('change', (event) => {
  if (event.target.closest(cartSelectors.cartQuantity)) {
    onQuantityChange(event);
  }
});
