import { load } from '@shopify/theme-sections';
import '../sections/products-slider-projects';
import '../sections/review-section';

load('*');

const categories = document.querySelectorAll('.projects-categories .cat');
const wrapper = document.querySelector("[data-role='project-categories']");
const catList = document.querySelector('.projects-categories');
const init = document.querySelector('.projects-block__wrapper-categories .init');

categories.forEach((cat) => {
  cat.addEventListener('click', (e) => {
    e.preventDefault();
    const _this = e.target;
    if (_this.classList.contains('_active')) return;

    document.querySelector('.projects-categories .cat._active').classList.remove('_active');
    _this.classList.add('_active');

    const list = document.querySelector('.projects-list');
    const listHeight = list.offsetHeight;
    list.style.minHeight = listHeight + 'px';

    const catNumber = _this.dataset.projcat;
    const projects = document.querySelectorAll('.projects-list .project');
    projects.forEach((ele) => {
      ele.classList.remove('_active');
      const text = _this.textContent;
      init.textContent = text;

      setTimeout(() => {
        if (ele.dataset.project === catNumber || catNumber === 'all') ele.classList.add('_active');
      }, 300);
    });

    setTimeout(() => {
      list.style.minHeight = 'auto';
    }, 300);

  });
});

wrapper.addEventListener('click', (e) => {
  const container = e.currentTarget;

  if (container.classList.contains('open')) {
    catList.style.maxHeight = null;
    container.classList.remove('open');

  } else {
    catList.style.maxHeight = `${catList.scrollHeight}px`;
    container.classList.add('open');
  }
});

window.addEventListener('resize', (e) => {
  catList.style.maxHeight = null;
  wrapper.classList.remove('open');
});
