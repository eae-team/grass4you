import { load } from '@shopify/theme-sections';
import { breaks } from '../components/utils';
import '../sections/products-slider-contacts';
import '../sections/review-section';

load('*');

const wrapper = document.querySelector("[data-role='faqs-categories']");
const catList = document.querySelector('.faqs-categories');
const categories = document.querySelectorAll('.cat');
const faq = document.getElementsByClassName('accordion');
const init = document.querySelector('.faqs-block__wrapper-categories .init');
let i;

categories.forEach((cat) => {
  cat.addEventListener('click', (e) => {
    e.preventDefault();
    const _this = e.target;
    if (_this.classList.contains('_active')) return;

    const catNumber = _this.dataset.cat;
    const faqQuestions = document.querySelector(`[data-block="${catNumber}"]`);

    document.querySelector('.faqs-categories .cat._active').classList.remove('_active');
    _this.classList.add('_active');

    const list = document.querySelector('.faqs-block__wrapper-questions');
    const listHeight = list.offsetHeight;
    list.style.minHeight = listHeight + 'px';
    document.querySelector('.faqs-questions._active').classList.remove('_active');
    const last = document.querySelector('.faqs-last._active');
    if(last) last.classList.remove('_active');

    const text = _this.textContent;
    init.textContent = text;

    setTimeout(() => {
      faqQuestions.classList.add('_active');
      if(last) last.classList.add('_active');
      list.style.minHeight = 'auto';
    }, 300);
  });
});

for (i = 0; i < faq.length; i++) {
  faq[i].addEventListener('click', (e) => {
    e.preventDefault();
    const _this = e.currentTarget;
    const answer = _this.nextElementSibling;

    if (_this.classList.contains('_active')) {
      _this.classList.remove('_active');
      answer.style.maxHeight = null;
      _this.querySelector('.colapsible-title div .open').classList.add('_active');
      _this.querySelector('.colapsible-title div .close').classList.remove('_active');
    } else {
      const active = document.querySelectorAll('.accordion._active');
      if (active.length > 0) {
        active.forEach((ele) => {
          ele.classList.remove('_active');
          ele.nextElementSibling.style.maxHeight = null;
          ele.querySelector('.colapsible-title div .open').classList.add('_active');
          ele.querySelector('.colapsible-title div .close').classList.remove('_active');
        });
      }
      _this.classList.add('_active');
      answer.style.maxHeight = `${answer.scrollHeight}px`;
      _this.querySelector('.colapsible-title div .open').classList.remove('_active');
      _this.querySelector('.colapsible-title div .close').classList.add('_active');
    }
  });
}

wrapper.addEventListener('click', (e) => {
  const container = e.currentTarget;

  if (container.classList.contains('open')) {
    catList.style.maxHeight = null;
    container.classList.remove('open');

  } else {
    catList.style.maxHeight = `${catList.scrollHeight}px`;
    container.classList.add('open');
  }
});

window.addEventListener('resize', (e) => {
  catList.style.maxHeight = null;
  wrapper.classList.remove('open');
});
