import { load } from '@shopify/theme-sections';
import Swiper, { Navigation, Pagination } from 'swiper';
import { breaks } from '../components/utils';
import '../sections/product';
import '../sections/products-slider-product';
import '../sections/product-project-block';

load('*');

window.addEventListener('load', () => {
  const selectors = {
    sliderScope: '.product__thumbnail-container',
    swiperProjects: '.product__thumbnail-container',
  };

  const windowWidth = window.innerWidth;
  window.swiperProjects = null;

  if (windowWidth <= breaks.medium) {
    window.swiperProjects = sliderSwiper(selectors.swiperProjects);
  }

  window.addEventListener('resize', resize);

  function sliderSwiper(element) {
    Swiper.use([Navigation, Pagination]);
    return new Swiper(element, {
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        bulletActiveClass: 'product__bullet-active',
        bulletClass: 'product__bullet',
        clickable: true,
      },
    });
  }

  function resize(event) {
    const width = event.target.innerWidth;
    if (width <= breaks.medium && !window.swiperProjects) {
      window.swiperProjects = sliderSwiper(selectors.swiperProjects);
    } else if (width > breaks.medium && window.swiperProjects) {
      window.swiperProjects.destroy(true, true);
      window.swiperProjects = null;
    }
  }
});

window.addEventListener('unload', () => {
  window.swiperProjects.destroy(true, true);
});
