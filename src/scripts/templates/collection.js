import axios from 'axios';
import { blockContent, unblockContent } from '../components/utils';

const collection = document.querySelector('[data-collection-handle]');
let currentCollection = collection.dataset.collectionHandle;

const productsHolder = document.querySelector('[data-role="collection-holder"]');
const selectGroup = document.querySelector('[data-action="category-filter"]');
const selectSort = document.querySelector('[data-action="sort-select"]');
const groupCollectionsAll = document.querySelectorAll('[data-role="group-item"]');
const collectionButtonAll = document.querySelectorAll('[data-action="select-collection"]');
const collectionFilterAll = document.querySelectorAll('[data-collection-filter]');

const collectionTitle = document.querySelector('[data-collection-title]');
const collectionDescription = document.querySelector('[data-collection-description]');

// filters
const filterClear = document.querySelectorAll('[data-action="clear-filters"]');
const filterToggleFilter = document.querySelectorAll('[data-action="toggle-collapse"]');
const filterOptionAll = document.querySelectorAll('[data-action-filter-option]');
const filterOpenMobile = document.querySelector('[data-action="open-filters-mobile"]');
const filterContainer = document.querySelector('[data-role-filter-container]');
const filterCloseMobile = document.querySelectorAll('[data-action="close-filters"]');

let currentPage = 1;
let filters = {};
let fetching = false;
let currentSort = 'manual';

if (selectSort) {
  selectSort.addEventListener('change', (event) => {
    currentSort = event.target.value;
    fetchProducts(currentPage);
  });
}

if (selectGroup) {
  selectGroup.addEventListener('change', () => {
    groupCollectionsAll.forEach((item) => {
      item.classList.add('_hide');
      item.classList.remove('_selected');
    });
    const groupCollections = document.querySelectorAll(`[data-group="${selectGroup.value}"]`);
    groupCollections.forEach((item, index) => {
      if (index === 0) {
        item.classList.add('_selected');
        currentCollection = item.dataset.handle;
      }
      item.classList.remove('_hide');
    });
    updateShowFilters();
    fetchProducts(currentPage);
  });
}

filterCloseMobile.forEach((item) => {
  item.addEventListener('click', (e) => {
    e.preventDefault();
    filterContainer.classList.remove('_open-filter');
    unblockContent();
  });
});

filterOpenMobile.addEventListener('click', (e) => {
  e.preventDefault();
  const isOpen = filterContainer.classList.contains('_open-filter');
  if (isOpen) {
    filterContainer.classList.remove('_open-filter');
    unblockContent();
  } else {
    filterContainer.classList.add('_open-filter');
    blockContent();
  }
});

filterClear.forEach((item) => {
  item.addEventListener('click', (e) => {
    e.preventDefault();
    clearFilters();
  });
});

filterToggleFilter.forEach((option) => {
  option.addEventListener('click', (event) => {
    event.stopPropagation();
    event.preventDefault();
    const target = event.target;
    const parent = target.parentNode;
    const tag = target.dataset.tag;
    const isOpen = parent.classList.contains('_open');
    const optionsGroup = parent.querySelector(`[data-filter-key="${tag}"]`);

    if (isOpen) {
      optionsGroup.style.height = '0px';
      optionsGroup.style.opacity = '0';
      parent.classList.remove('_open');
    } else {
      optionsGroup.style.height = 'auto';
      const height = `${optionsGroup.scrollHeight}px`;
      optionsGroup.style.height = '0px';

      setTimeout(() => {
        optionsGroup.style.height = height;
        optionsGroup.style.opacity = '1';
        parent.classList.add('_open');
      }, 100);
    }
  });
});

filterOptionAll.forEach((option) => {
  option.addEventListener('change', (event) => {
    const filter = event.target;
    const key = filter.dataset.key;
    const value = filter.dataset.value;

    if (filter.checked) {
      const groupCurrentFilter = filter.parentNode.querySelectorAll(`[data-key="${key}"]`);
      groupCurrentFilter.forEach((item) => {
        if (item.dataset.value !== value) {
          item.checked = false;
        }
      });

      filters[key] = value;
    } else {
      delete filters[key];
    }
    currentPage = 1;
    fetchProducts(currentPage);
  });
});

collectionButtonAll.forEach((item) => {
  item.addEventListener('click', (event) => {
    groupCollectionsAll.forEach((element) => {
      element.classList.remove('_selected');
    });
    const collectionButton = event.target;
    collectionButton.classList.add('_selected');
    currentCollection = collectionButton.dataset.handle;
    updateShowFilters();
    clearFilters();
  });
});

function init() {
  let auxPage = currentPage;
  const currentURL = window.location.href;
  const objectURL = new URL(currentURL);
  const pathnameArray = objectURL.pathname.split('/');
  const auxCollection = pathnameArray[2];

  currentCollection = auxCollection;

  const collectionElement = document.querySelector(`[data-handle="${currentCollection}"]`);
  if (collectionElement) {
    const groupID = collectionElement.dataset.group;
    selectGroup.value = groupID;

    groupCollectionsAll.forEach((item) => {
      item.classList.add('_hide');
      item.classList.remove('_selected');
    });

    const groupCollections = document.querySelectorAll(`[data-group="${groupID}"]`);
    groupCollections.forEach((item) => {
      item.classList.remove('_hide');
    });

    collectionElement.classList.add('_selected');
    updateShowFilters();
  }

  if (typeof pathnameArray[3] !== 'undefined') {
    const filtersArray = pathnameArray[3].split('+');
    filtersArray.forEach((item) => {
      const keyArray = item.split('_');
      filters[keyArray[0]] = keyArray[1];

      const accordionElement = document.querySelector(`[data-tag="${keyArray[0]}"]`);
      if (accordionElement) {
        accordionElement.classList.add('_open');
      }

      const optionsElement = document.querySelector(`[data-filter-key="${keyArray[0]}"]`);
      if (optionsElement) {
        optionsElement.style.height = 'auto';
        const height = `${optionsElement.scrollHeight}px`;
        optionsElement.style.height = height;
        optionsElement.style.opacity = '1';
        const optionsGroup = optionsElement.querySelector(`[data-value="${keyArray[1]}"]`);
        optionsGroup.checked = true;
      }
    });
  }

  if (objectURL.searchParams.has('page')) {
    auxPage = objectURL.searchParams.getAll('page')[0];
  }

  if (objectURL.searchParams.has('sort_by')) {
    currentSort = objectURL.searchParams.getAll('sort_by')[0];
    selectSort.value = currentSort;
  }

  fetchProducts(auxPage);
}

function updateShowFilters() {
  collectionFilterAll.forEach((item) => {
    item.classList.add('_hide');
    item.classList.remove('_show');
  });
  const collectionFilterSelected = document.querySelectorAll(`[data-collection="${currentCollection}"]`);
  collectionFilterSelected.forEach((item) => {
    item.classList.remove('_hide');
    item.classList.add('_show');
  });
}

function clearFilters() {
  filters = {};
  filterOptionAll.forEach((option) => {
    option.checked = false;
  });
  filterToggleFilter.forEach((option) => {
    option.classList.remove('_open');
  });
  document.querySelectorAll('[data-role-filter-options]').forEach((option) => {
    option.style.height = '0px';
    option.style.opacity = '0';
  });
  updateShowFilters();
  fetchProducts(1);
}

function initPagination() {
  // pagination
  const paginationPageAll = document.querySelectorAll('[data-action-page]');
  const paginationPrevious = document.querySelector('[data-action-previous]');
  const paginationNext = document.querySelector('[data-action-next]');

  if (paginationNext) {
    paginationNext.addEventListener('click', (event) => {
      event.stopPropagation();
      event.preventDefault();
      fetchProducts(parseInt(currentPage, 10) + 1);
    });
  }

  if (paginationPrevious) {
    paginationPrevious.addEventListener('click', (event) => {
      event.stopPropagation();
      event.preventDefault();
      fetchProducts(parseInt(currentPage, 10) - 1);
    });
  }

  if (paginationPageAll) {
    paginationPageAll.forEach((item) => {
      item.addEventListener('click', (event) => {
        event.stopPropagation();
        event.preventDefault();
        const target = event.target;
        const index = target.dataset.indexPage;
        fetchProducts(index);
      });
    });
  }
}

function fetchProducts(page) {
  if (fetching) {
    return;
  }

  fetching = true;
  const activeFilters = [];
  let path = `/collections/${currentCollection}`;
  currentPage = page;

  for (const item in filters) {
    if (item) {
      activeFilters.push(`${item}_${filters[item]}`);
    }
  }

  if (activeFilters.length) {
    path += `/${activeFilters.join('+')}`;
  }
  const query = `page=${currentPage}&sort_by=${currentSort}&view=json`;

  document.body.classList.add('_loading');

  if (page === 1) {
    productsHolder.classList.add('_hide');
  }

  axios.get(`${path}?${query}`)
    .then((response) => {
      // handle success
      const resultObject = response.data;
      const products = resultObject.body;

      if (currentPage === 1) {
        productsHolder.innerHTML = '';
      }

      if (collectionTitle) {
        collectionTitle.innerHTML = resultObject.collectionTitle;
      }

      if (collectionDescription && resultObject.collectionDescription) {
        collectionDescription.innerHTML = resultObject.collectionDescription;
        collectionDescription.classList.add('collection--show');
      } else {
        collectionDescription.innerHTML = '';
        collectionDescription.classList.remove('collection--show');
      }

      productsHolder.innerHTML = products;
      document.body.classList.remove('_loading');
      fetching = false;
      productsHolder.classList.remove('_hide');

      window.history.pushState({}, '', `${path}?page=${currentPage}&sort_by=${currentSort}`);
      initPagination();
      return true;
    })
    .catch((error) => {
      // handle error
      fetching = false;
    });
}

init();
