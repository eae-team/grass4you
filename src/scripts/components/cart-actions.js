import axios from 'axios';
import * as cart from '@shopify/theme-cart';
import { blockContent, unblockContent } from '../components/utils';

const cartQuantity = document.querySelectorAll('[data-role="cart-qtd"]');
const cartTotal = document.querySelector('[data-role="cart-total"]');
const cartItems = document.querySelector('[data-role="cart-items"]');
const checkoutEl = document.querySelector('[data-action="go-checkout"]');

export function openCart() {
  document.body.classList.add('_quick-cart-open');
  blockContent();
}

export function closeCart() {
  document.body.classList.remove('_quick-cart-open');
  unblockContent();
}

export function updateCart(cartElement) {
  if (window.BOLD && BOLD.common && BOLD.common.cartDoctor && typeof BOLD.common.cartDoctor.fix === 'function') {
    cartElement = BOLD.common.cartDoctor.fix(cartElement);
  }

  axios.get('/cart?view=json')
    .then((response) => {
      // handle success
      const cartData = response.data;
      cartQuantity.forEach((item) => {
        item.innerHTML = cartData.cartCount;
      });
      cartItems.innerHTML = cartData.cartItems;
      checkoutEl.classList.toggle(
        'disabled',
        !(cartData.cartCount !== 0)
      );
      cartTotal.innerHTML = cartData.cartTotal;
      if (window.BOLD && BOLD.common && BOLD.common.eventEmitter && typeof BOLD.common.eventEmitter.emit === 'function') {
        BOLD.common.eventEmitter.emit('BOLD_COMMON_cart_loaded', cartElement);
        BOLD.common.eventEmitter.emit('BOLD_COMMON_redirect_upsell_product');
      }
      return true;
    })
    .catch((error) => {
      // handle error
    });
}

export function removeCartLine(lineKey) {
  return cart.removeItem(lineKey).then(async () => {
    const cartElement = await getCart();
    return updateCart(cartElement);
  });
}

export function addToCart(variantId, quantity, properties) {
  return cart
    .addItem(Number(variantId), { quantity, properties })
    .then(async () => {
      const cartElement = await getCart();
      updateCart(cartElement);
      return quantity;
    })
    .catch((response) => {
      return response.status === 422 ? 0 : -1;
    });
}

export function updateItemCart(key, quantity, properties) {
  return cart
    .updateItem(key, { quantity, properties })
    .then(async () => {
      const cartElement = await getCart();
      updateCart(cartElement);
      return quantity;
    })
    .catch((response) => {
      return response.status === 422 ? 0 : -1;
    });
}

export function getCart() {
  return cart.getState()
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
    });
}
