import * as cartActions from './cart-actions';

const cartSelectors = {
  cart: '[data-role="quick-cart"]',
  openQuickCart: '[data-action="open-quick-cart"]',
  closeQuickCart: '[data-action="close"]',
  removeCartLine: '[data-action="remove-cart-line"]',
  quickQuantityDown: '[data-action="quick-quantity-down"]',
  quickQuantityUp: '[data-action="quick-quantity-up"]',
  quickQuantity: '[data-role="quick-quantity"]',
};

const cart = document.querySelector(cartSelectors.cart);

function openQuickCartHandler(event) {
  event.preventDefault();
  cartActions.openCart();
}

function closeQuickCartHandler(event) {
  event.preventDefault();
  cartActions.closeCart();
}

// cart.addEventListener('click', (event) => {
//   if (cart.get(0) === event.target) {
//     cartActions.closeCart();
//   }
// });

function removeCartLineHandler(event) {
  event.preventDefault();
  cart.classList.add('_loading');
  cartActions
    .removeCartLine(event.target.dataset.cartLineKey)
    .then(() => {
      cart.classList.remove('_loading');
      return true;
    })
    .catch((error) => {
      console.log(error);
    });
}

function onQuantityClick(event, quantity) {
  const target = event.target;
  const productId = target.dataset.id;
  const quantityInput = cart.querySelector(`[data-cart-product-id="${productId}"]`);
  const currentValue = parseInt(quantityInput.value, 10);
  let newVal;
  if ((currentValue + quantity) < 1) {
    newVal = 1;
  } else {
    newVal = currentValue + quantity;
  }
  quantityInput.value = newVal;

  const productKey = quantityInput.dataset.cartKey;
  cartActions
    .updateItemCart(productKey, newVal, {})
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.log(error);
    });
}

function onQuantityChange(event) {
  const quantityInput = event.target;
  const currentValue = parseInt(quantityInput.value, 10);
  let newVal = currentValue;
  if ((currentValue) < 1) {
    newVal = 1;
  }
  quantityInput.value = newVal;

  const productKey = quantityInput.dataset.cartKey;
  cartActions
    .updateItemCart(productKey, newVal, {})
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.log(error);
    });
}

document.addEventListener('click', (event) => {
  if (event.target.closest(cartSelectors.openQuickCart)) {
    openQuickCartHandler(event);
  } else if (event.target.closest(cartSelectors.closeQuickCart)) {
    closeQuickCartHandler(event);
  } else if (event.target.closest(cartSelectors.removeCartLine)) {
    removeCartLineHandler(event);
  } else if (event.target.closest(cartSelectors.quickQuantityDown)) {
    onQuantityClick(event, -1);
  } else if (event.target.closest(cartSelectors.quickQuantityUp)) {
    onQuantityClick(event, 1);
  }
});

document.addEventListener('change', (event) => {
  if (event.target.closest(cartSelectors.quickQuantity)) {
    onQuantityChange(event);
  }
});
