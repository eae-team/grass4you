import Swiper, { Navigation, Pagination } from 'swiper';

export const ArticleBlogMobile = (swiperScope) => {
  Swiper.use([Navigation, Pagination]);

  return new Swiper(swiperScope, {
    slidesPerView: 1,
    spaceBetween: 10,
    initialSlide: 0,
    autoHeight: true,
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
    },
  });
}