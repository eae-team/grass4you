import Swiper, { Navigation, Pagination } from 'swiper';

export const sliderProducts = (swiperScope) => {
  Swiper.use([Navigation, Pagination]);

  return new Swiper(swiperScope, {
    slidesPerView: 1,
    centered: true,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      bulletActiveClass: 'products-slider__bullet-active',
      bulletClass: 'products-slider__bullet',
      clickable: true,
    },
    navigation: {
      nextEl: '.products-slider__button-next',
      prevEl: '.products-slider__button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1300: {
        slidesPerView: 4,
      },
    },
  });
}
