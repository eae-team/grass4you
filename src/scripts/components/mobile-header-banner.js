import Swiper, { Navigation, Pagination } from 'swiper';

export const sliderBannerMobile = (swiperScope) => {
  Swiper.use([Navigation, Pagination]);

  return new Swiper(swiperScope, {
    slidesPerView: 'auto',
    spaceBetween: 10,
    centeredSlides: true,
    autoHeight: true,
  });
}