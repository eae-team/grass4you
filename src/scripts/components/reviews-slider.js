import Swiper, { Navigation, Pagination } from 'swiper';

export const sliderReviews = (swiperScope) => {
  Swiper.use([Navigation, Pagination]);
  return new Swiper(swiperScope, {
    slidesPerView: 3,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      bulletActiveClass: 'review-slider__bullet-active',
      bulletClass: 'review-slider__bullet',
      clickable: true,
    },
    navigation: {
      nextEl: '.review-slider__button-next',
      prevEl: '.review-slider__button-prev',
    },
    breakpoints: {
      990: {
        slidesPerView: 3,
      },
      768: {
        slidesPerView: 2,
      },
      320: {
        slidesPerView: 1,
      },
    },
  });
}
