export const breaks = {
  small: 750,
  medium: 990,
  large: 1400,
};

export function blockContent() {
  const top = document.scrollTop;
  window.headroom.freeze();
  document.body.classList.add('_block-content');
  document.body.style.top = `${top * -1}px`;
}

export function unblockContent() {
  const top = document.body.getBoundingClientRect().top;
  document.body.classList.remove('_block-content');
  document.scrollTop = top * -1;
  setTimeout(() => {
    window.headroom.unfreeze();
  }, 100);
}

export function openMenu() {
  if (document.body.classList.contains('_menu-open')) {
    return;
  }
  document.body.classList.add('_menu-open');
  blockContent();
}

export function closeMenu() {
  document.body.classList.remove('_menu-open');
  unblockContent();
}

export function toggleMenu() {
  if (document.body.classList.contains('_menu-open')) {
    closeMenu();
  } else {
    openMenu();
  }
}
