import Swiper, { Navigation, Pagination } from 'swiper';

export const sliderBanner = (swiperScope) => {
  Swiper.use([Navigation, Pagination]);

  return new Swiper(swiperScope, {
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      bulletActiveClass: 'header-banner__bullet-active',
      bulletClass: 'header-banner__bullet',
      clickable: true,
    },
    navigation: {
      nextEl: '.header-banner__button-next',
      prevEl: '.header-banner__button-prev',
    },
  });
};
